#include "os_scheduler.h"
#include "util.h"
#include "os_input.h"
#include "os_scheduling_strategies.h"
#include "os_taskman.h"
#include "os_core.h"
#include "lcd.h"

#include <avr/interrupt.h>

//----------------------------------------------------------------------------
// Private Types
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Globals
//----------------------------------------------------------------------------

//! Array of states for every possible process
//#warning IMPLEMENT STH. HERE
Process os_processes [MAX_NUMBER_OF_PROCESSES];

//! Array of function pointers for every registered program
//#warning IMPLEMENT STH. HERE
Program *os_programs[MAX_NUMBER_OF_PROGRAMS];

//! Index of process that is currently executed (default: idle)
//#warning IMPLEMENT STH. HERE
ProcessID currentProc;
//----------------------------------------------------------------------------
// Private variables
//----------------------------------------------------------------------------

//! Currently active scheduling strategy
//#warning IMPLEMENT STH. HERE
SchedulingStrategy currentSchedulingStrategy;

//! Count of currently nested critical sections
//#warning IMPLEMENT STH. HERE
uint8_t criticalSectionCount;

//! Used to auto-execute programs.
uint16_t os_autostart;

//----------------------------------------------------------------------------
// Private function declarations
//----------------------------------------------------------------------------

//! ISR for timer compare match (scheduler)
ISR(TIMER2_COMPA_vect) __attribute__((naked));

//----------------------------------------------------------------------------
// Function definitions
//----------------------------------------------------------------------------

/*!
 *  Timer interrupt that implements our scheduler. Execution of the running
 *  process is suspended and the context saved to the stack. Then the periphery
 *  is scanned for any input events. If everything is in order, the next process
 *  for execution is derived with an exchangeable strategy. Finally the
 *  scheduler restores the next process for execution and releases control over
 *  the processor to that process.
 */
ISR(TIMER2_COMPA_vect) {
//#warning IMPLEMENT STH. HERE

        // 2. Sichern des Laufzeitkontextes des aktuellen Prozesses auf dessen Prozessstack
        saveContext();

        // 3. Sichern des Stackpointers für den Prozessstack des aktuellen Prozesses
        os_processes[os_getCurrentProc()].sp.as_int = SP;

        // Speichern der aktuellen Stackchecksum
        os_processes[os_getCurrentProc()].checksum = os_getStackChecksum(os_getCurrentProc());

        // 4. Setzen des SP-Registers auf den Scheduler-Stack
        SP = BOTTOM_OF_ISR_STACK;

        //Aufruf des Taskmanagers
        if ((os_getInput() & 0b00001001) == 0b00001001){
            os_taskManMain();
            while ((os_getInput() & 0b00001001) == 0b00001001) {}
        }

        // 5. Setzen des Prozesszustandes des aktuellen Prozesses auf OS_PS_READY
        os_processes[os_getCurrentProc()].state = OS_PS_READY;

        // 6. Auswahl des nächsten fortzusetzenden Prozesses durch Aufruf der aktuell verwendeten Schedulingstrategie
        ProcessID nextProcess = 0;
        switch (os_getSchedulingStrategy()) {
            case OS_SS_EVEN:
                nextProcess = os_Scheduler_Even(os_processes, os_getCurrentProc());
            break;
            case OS_SS_RANDOM:
                nextProcess = os_Scheduler_Random(os_processes, os_getCurrentProc());
            break;
            case OS_SS_RUN_TO_COMPLETION:
                nextProcess = os_Scheduler_RoundRobin(os_processes, os_getCurrentProc());
            break;
            case OS_SS_ROUND_ROBIN:
                nextProcess = os_Scheduler_InactiveAging(os_processes, os_getCurrentProc());
            break;
            case OS_SS_INACTIVE_AGING:
                nextProcess = os_Scheduler_RunToCompletion(os_processes, os_getCurrentProc());
            break;
        }

        // Next process will run
        currentProc = nextProcess;

        // 7. Setzen des Prozesszustandes des fortzusetzenden Prozesses auf OS_PS_RUNNING
        os_processes[nextProcess].state = OS_PS_RUNNING;

        // 8. Wiederherstellen des Stackpointers für den Prozessstack des fortzusetzenden Prozesses
        SP = os_processes[nextProcess].sp.as_int;

        if(os_processes[nextProcess].checksum != os_getStackChecksum(nextProcess)) os_error("Stackinkonsistenz erkannt");

        // 9. Wiederherstellen des Laufzeitkontextes des fortzusetzenden Prozesses durch das Assemblermakro restoreContext()
        //automatischer Rücksprung durch das Assemblermakro restoreContext()
        restoreContext();
}

/*!
 *  Used to register a function as program. On success the program is written to
 *  the first free slot within the os_programs array (if the program is not yet
 *  registered) and the index is returned. On failure, INVALID_PROGRAM is returned.
 *  Note, that this function is not used to register the idle program.
 *
 *  \param program The function you want to register.
 *  \return The index of the newly registered program.
 */
ProgramID os_registerProgram(Program* program) {
    ProgramID slot = 0;

    // Find first free place to put our program
    while (os_programs[slot] &&
           os_programs[slot] != program && slot < MAX_NUMBER_OF_PROGRAMS) {
        slot++;
    }

    if (slot >= MAX_NUMBER_OF_PROGRAMS) {
        return INVALID_PROGRAM;
    }

    os_programs[slot] = program;
    return slot;
}

/*!
 *  Used to check whether a certain program ID is to be automatically executed at
 *  system start.
 *
 *  \param programID The program to be checked.
 *  \return True if the program with the specified ID is to be auto started.
 */
bool os_checkAutostartProgram(ProgramID programID) {
    return !!(os_autostart & (1 << programID));
}

/*!
 *  This is the idle program. The idle process owns all the memory
 *  and processor time no other process wants to have.
 */
PROGRAM(0, AUTOSTART) {
    //#warning IMPLEMENT STH. HERE
    while(true) {
        lcd_writeProgString(PSTR("."));
        delayMs(DEFAULT_OUTPUT_DELAY);
    }
}

/*!
 * Lookup the main function of a program with id "programID".
 *
 * \param programID The id of the program to be looked up.
 * \return The pointer to the according function, or NULL if programID is invalid.
 */
Program* os_lookupProgramFunction(ProgramID programID) {
    // Return NULL if the index is out of range
    if (programID >= MAX_NUMBER_OF_PROGRAMS) {
        return NULL;
    }

    return os_programs[programID];
}

/*!
 * Lookup the id of a program.
 *
 * \param program The function of the program you want to look up.
 * \return The id to the according slot, or INVALID_PROGRAM if program is invalid.
 */
ProgramID os_lookupProgramID(Program* program) {
    ProgramID i;

    // Search program array for a match
    for (i = 0; i < MAX_NUMBER_OF_PROGRAMS; i++) {
        if (os_programs[i] == program) {
            return i;
        }
    }

    // If no match was found return INVALID_PROGRAM
    return INVALID_PROGRAM;
}

/*!
 *  This function is used to execute a program that has been introduced with
 *  os_registerProgram.
 *  A stack will be provided if the process limit has not yet been reached.
 *  This function is multitasking safe. That means that programs can repost
 *  themselves, simulating TinyOS 2 scheduling (just kick off interrupts ;) ).
 *
 *  \param programID The program id of the program to start (index of os_programs).
 *  \param priority A priority ranging 0..255 for the new process:
 *                   - 0 means least favorable
 *                   - 255 means most favorable
 *                  Note that the priority may be ignored by certain scheduling
 *                  strategies.
 *  \return The index of the new process or INVALID_PROCESS as specified in
 *          defines.h on failure
 */
ProcessID os_exec(ProgramID programID, Priority priority) {
    //#warning IMPLEMENT STH. HERE

    os_enterCriticalSection();

    //find first available slot in os_processes using the state of the entries
    uint8_t procIndex = 0;
    while(procIndex < MAX_NUMBER_OF_PROCESSES){
        if(os_processes[procIndex].state == OS_PS_UNUSED) break;
        procIndex += 1;
    }
    //return INVALID_PROCESS if no slots are available
    if(procIndex >= MAX_NUMBER_OF_PROCESSES) {
        os_leaveCriticalSection();
        return INVALID_PROCESS;
    }

    //Load the function pointer correspondent to the given PID from os_programs
    Program *programPointer = os_lookupProgramFunction(programID);
    //return INVALID_PROCESS if a null pointer is found
    if(programPointer == NULL) {
        os_leaveCriticalSection();
        return INVALID_PROCESS;
    }
    //Save the PID, priority and state of the process
    os_processes[procIndex].priority = priority;
    os_processes[procIndex].progID = programID;
    os_processes[procIndex].state = OS_PS_READY;

    //Prepare the stack

    //Get the first address of the stack pointing towards the right place
    //Extract the Low-Byte of the address saved in programPointer and save it at the bottom of the stack
    os_processes[procIndex].sp.as_int = PROCESS_STACK_BOTTOM(procIndex);
    *(os_processes[procIndex].sp.as_ptr) = (uint8_t)((uint16_t) programPointer);
    os_processes[procIndex].sp.as_int -= 1; // jump to next pointer

    //Do the same to the second Byte of the address saved in programPointer
    *(os_processes[procIndex].sp.as_ptr) = (uint8_t)(((uint16_t) programPointer) >> 8);
    os_processes[procIndex].sp.as_int -= 1; // jump to next pointer

    // Set all other 33 registers / stacks to null pointers
    for(uint8_t j = 1; j <= 33; j++){
        *(os_processes[procIndex].sp.as_ptr) = 0;
        os_processes[procIndex].sp.as_int -= 1;
    }

    os_processes[procIndex].checksum = os_getStackChecksum(procIndex);

    os_leaveCriticalSection();

    return procIndex;
}

/*!
 *  If all processes have been registered for execution, the OS calls this
 *  function to start the idle program and the concurrent execution of the
 *  applications.
 */
void os_startScheduler(void) {
    //#warning IMPLEMENT STH. HERE
    currentProc = 0; // Set Variable of current process to 0 (Leerlaufprozess)
    os_processes[os_getCurrentProc()].state = OS_PS_RUNNING; // Set state of leerlaufprozess to running
    // Setzen des Stackpointers auf den Prozessstack des Leerlaufprozesses
    SP = os_processes[os_getCurrentProc()].sp.as_int;
    restoreContext();
}

/*!
 *  In order for the Scheduler to work properly, it must have the chance to
 *  initialize its internal data-structures and register.
 */
void os_initScheduler(void) {
    //#warning IMPLEMENT STH. HERE
    uint8_t id;

    // Set states of all PROCESSES in array to OS_PS_UNUSED
    for ( id = 0; id < MAX_NUMBER_OF_PROCESSES; id++) {
        os_processes[id].state = OS_PS_UNUSED;
    }

    // look up PROGRAMS with Autostart and start them
    for ( id = 0; id < MAX_NUMBER_OF_PROGRAMS; id++) {
        if (os_checkAutostartProgram(id)) {
            os_exec(id, DEFAULT_PRIORITY);
        }
    }

}

/*!
 *  A simple getter for the slot of a specific process.
 *
 *  \param pid The processID of the process to be handled
 *  \return A pointer to the memory of the process at position pid in the os_processes array.
 */
Process* os_getProcessSlot(ProcessID pid) {
    return os_processes + pid;
}

/*!
 *  A simple getter for the slot of a specific program.
 *
 *  \param programID The ProgramID of the process to be handled
 *  \return A pointer to the function pointer of the program at position programID in the os_programs array.
 */
Program** os_getProgramSlot(ProgramID programID) {
    return os_programs + programID;
}

/*!
 *  A simple getter to retrieve the currently active process.
 *
 *  \return The process id of the currently active process.
 */
ProcessID os_getCurrentProc(void) {
    //#warning IMPLEMENT STH. HERE
    return currentProc;
}

/*!
 *  This function return the the number of currently active process-slots.
 *
 *  \returns The number currently active (not unused) process-slots.
 */
uint8_t os_getNumberOfActiveProcs(void) {
    uint8_t num = 0;

    ProcessID i = 0;
    do {
        num += os_getProcessSlot(i)->state != OS_PS_UNUSED;
    } while (++i < MAX_NUMBER_OF_PROCESSES);

    return num;
}

/*!
 *  This function returns the number of currently registered programs.
 *
 *  \returns The amount of currently registered programs.
 */
uint8_t os_getNumberOfRegisteredPrograms(void) {
    uint8_t count = 0;
    for (ProcessID i = 0; i < MAX_NUMBER_OF_PROGRAMS; i++)
        if (*(os_getProgramSlot(i))) count++;
    // Note that this only works because programs cannot be unregistered.
    return count;
}

/*!
 *  Sets the current scheduling strategy.
 *
 *  \param strategy The strategy that will be used after the function finishes.
 */
void os_setSchedulingStrategy(SchedulingStrategy strategy) {
    //#warning IMPLEMENT STH. HERE
    currentSchedulingStrategy = strategy;
}

/*!
 *  This is a getter for retrieving the current scheduling strategy.
 *
 *  \return The current scheduling strategy.
 */
SchedulingStrategy os_getSchedulingStrategy(void) {
    //#warning IMPLEMENT STH. HERE
    return currentSchedulingStrategy;
}

/*!
 *  Enters a critical code section by disabling the scheduler if needed.
 *  This function stores the nesting depth of critical sections of the current
 *  process (e.g. if a function with a critical section is called from another
 *  critical section) to ensure correct behavior when leaving the section.
 *  This function supports up to 255 nested critical sections.
 */
void os_enterCriticalSection(void) {
    //#warning IMPLEMENT STH. HERE
    //1. save global interrupt bit locally
    uint8_t globalInterruptBit = (SREG & 0b10000000);

    //2. Disable global interrupt enable bit
    SREG &= 0b01111111;

    // 3. Inkrementieren der Verschachtelungstiefe des kritischen Bereiches
    criticalSectionCount += 1;

    // 4. Deaktivieren des Schedulers
    TIMSK2 &= 0b11111101;

    //5. Re-enable state of global interrupt
    SREG |= globalInterruptBit;
}

/*!
 *  Leaves a critical code section by enabling the scheduler if needed.
 *  This function utilizes the nesting depth of critical sections
 *  stored by os_enterCriticalSection to check if the scheduler
 *  has to be reactivated.
 */
void os_leaveCriticalSection(void) {
    //#warning IMPLEMENT STH. HERE
    //1. save global interrupt bit locally
    uint8_t globalInterruptBit = (SREG & 0b10000000);

    //2. Disable global interrupt enable bit
    SREG &= 0b01111111;

    // 3. Dekrementieren der Verschachtelungstiefe des kritischen Bereiches
    criticalSectionCount -= 1;

    //4. Enable Scheduler if CriticalSectionCount is 0
    if(criticalSectionCount == 0)TIMSK2 |= 0b00000010;
    else if(criticalSectionCount < 0) os_errorPStr("os_leaveCriticalSection called upon more often than os_enterCriticalSection");

    //5. Re-enable state of global interrupt
    SREG |= globalInterruptBit;
}

/*!
 *  Calculates the checksum of the stack for a certain process.
 *
 *  \param pid The ID of the process for which the stack's checksum has to be calculated.
 *  \return The checksum of the pid'th stack.
 */
StackChecksum os_getStackChecksum(ProcessID pid) {
    //#warning IMPLEMENT STH. HERE
    StackPointer currentAddress;
    currentAddress.as_int = PROCESS_STACK_BOTTOM(pid);
    uint8_t checksum=0;
    while(currentAddress.as_int != os_processes[pid].sp.as_int) {
        checksum ^= *(currentAddress.as_ptr);
        currentAddress.as_ptr--;
    }
    return checksum;
}